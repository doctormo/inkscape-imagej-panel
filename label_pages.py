#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2024 Jerome Mutterer and Martin Owens
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Add labels with page number to all pages
"""

import inkex

def label_pages(self):
    # as seen in: https://gitlab.com/inklinea/page-watermark
    page_list = self.svg.xpath('//inkscape:page')
    i = 1
    for page in page_list:
        if not page.get('inkscape:label') or not self.options.keep:
            page.set('inkscape:label', self.options.prefix+str(i))
        i = i + 1
    return

class PagesLabels(inkex.EffectExtension):
    """Pages Labels"""

    def add_arguments(self, pars):
        pars.add_argument("--labelpages_notebook", type=str, dest="labelpages_notebook", default=0)
        pars.add_argument("--prefix", help="Label prefix", default="")
        pars.add_argument("--keep", help="Keep existing labels", type=inkex.Boolean, default=False)

    def effect(self):
        if int((inkex.__version__).split('.')[1]) < 3:
            inkex.errormsg('Only tested for Inkscape 1.3+')
            return
        label_pages(self)

if __name__ == '__main__':
    PagesLabels().run()
