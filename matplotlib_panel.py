#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2021 Jerome Mutterer and Martin Owens
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Request Image from Matplotlib
"""

import os
import sys
import shlex
import tempfile


from base64 import encodebytes

import inkex
from inkex.elements import Image, Rectangle
from inkex.command import call

from base64 import decodebytes

from ijmacro_panel import ImageJPanel

class MPLPanel(ImageJPanel):
    """Matplotlib Panel"""
    select_all = (Image, Rectangle)

    def add_arguments(self, pars):
        pars.add_argument("--tab", help="The selected UI-tab when OK was pressed")
        pars.add_argument("--pycmd", help="Python program", default="/usr/bin/python")
        pars.add_argument("--cmdopt", help="Python options", default="$MACRO")
        pars.add_argument("--dpi", type=float, default=300, help="+/-")

    def _process_image(self, ops, elem):
        index = self.index
        images_file = os.path.join(tempfile.gettempdir(), elem.eid + '.png').replace('\\', '/')
        macros_file = os.path.join(tempfile.gettempdir(), elem.eid + '.py').replace('\\', '/')
        
        code = self._get_code_from_lineage(elem)
        # if no desc available, add example code
        for child in elem.descendants():
            if isinstance(child, inkex.Desc):
                panelcode = child.text 
        if (code==""):
            code = """
import matplotlib.pyplot as plt
plt.plot([1, 2, 3, 4], [1, 4, 9, 16])

fs=12
plt.title('My Title',fontsize=fs)
plt.xlabel('Time',fontsize=fs)
plt.ylabel('Intensity',fontsize=fs)
plt.tick_params( labelsize=fs)
"""
            panelcode = code
        dpi = ops.dpi
        width = float(elem.width)
        height = float(elem.height)
        script = f"""
# matplotlib script
import matplotlib.pyplot
panelIndex = {index}

{code}

inkscapefigure = matplotlib.pyplot.gcf()
inkscapefigure.set_size_inches({width}/25.4, {height}/25.4)
matplotlib.pyplot.savefig("{images_file}", bbox_inches="tight",dpi={dpi}, transparent=True)

"""

        # Save the script to the rscript path
        with open(macros_file, 'w') as fhl:
            fhl.write(script)

        # Inject the script file path into the command
        cmdopt = ops.cmdopt.replace("$MACRO", macros_file)

        # We build the command from the program name plus arguments
        # The arguments are built as a list to maintain security

        done = call(ops.pycmd, *(shlex.split(cmdopt)))
       
        if not os.path.isfile(images_file):
            raise inkex.AbortExtension(f"Failed to save image file '{images_file}'")

        elem = self._elem_is_image(elem,"matplotlibpanel")
        elem.desc = panelcode

        self._embed_image(elem,images_file)


if __name__ == '__main__':
    MPLPanel().run()
